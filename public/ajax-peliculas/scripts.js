var request = new XMLHttpRequest()
request.open('GET', 'https://ghibliapi.herokuapp.com/films', true)
request.onload = function() {

  var respuesta = JSON.parse(this.response)
  var app = document.querySelector("div#root")

  respuesta.forEach(function(objeto, indice) {

    var contenedor = document.createElement('div')
    var titulo = document.createElement('div')
    titulo.innerHTML = objeto.title
    contenedor.appendChild(titulo)
    contenedor.classList.add('pelicula')
    titulo.classList.add('pelicula__titulo')

    var descripcion = document.createElement("div")
    descripcion.innerHTML = objeto.description
    contenedor.appendChild(descripcion)
    descripcion.classList.add('pelicula__descripcion')

    app.appendChild(contenedor)
  })

}
request.send()
//POST - CREATE
//GET - READ
//PUT - UPDATE
//DELETE - DELETE