# Portafolio Codellege

Este es mi portafolio de ejercicios realizados durante el curso Codellege 2019

#¿Qué hago?


1.- Crear la carpeta "public"
2.- Añadir un index.html para que sea la pagina de bienvenida
    - La idea es que sea la pagina principal y tenga una lista de links a todos
    los trabajos que hemos hecho.
3.- Añadir una carpeta por cada proyecto o ejercicio que hemos hecho.
4.- Guardar y enviar los cambios a gitlab, abre una terminal en tu carpeta y sigue
los siguientes comandos:
    - git add <nombre de tus archivos>
    - git add README.md public/index.html .gitlab-ci.yml
    - git commit -m 'Añadir pagina inicial'
    - git push

#¿Cómo reviso?

1.- Copia la URL de tu proyecto https://enriquealejandrogm.gitlab.io/codellege
